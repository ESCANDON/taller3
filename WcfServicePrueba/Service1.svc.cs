﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;



namespace WcfServicePrueba
{
  
    public class Service1 : IService1
    {
        private ModelAppwebContainer db = new ModelAppwebContainer();

        public List<Usuarios> MostrarTablaU()
        {
           return db.UsuariosSet.ToList();
        }

            public Usuarios MostrarUsuarios(string id)
        {
            int nId = Convert.ToInt16(id);
            return db.UsuariosSet.Where(p => p.Id == nId).First();
        }
    }
}
